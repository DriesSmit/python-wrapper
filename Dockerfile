
FROM ubuntu:18.04 AS build
ARG VERSION=16.0.0
WORKDIR /root

RUN apt update && \
    apt -y install autoconf bison clang flex libboost-dev libboost-all-dev libc6-dev make wget

RUN apt -y install build-essential libboost-all-dev qt5-default libfontconfig1-dev libaudio-dev libxt-dev libglib2.0-dev libxi-dev libxrender-dev libboost-all-dev

RUN wget https://github.com/rcsoccersim/rcssserver/archive/rcssserver-$VERSION.tar.gz && \
    tar xfz rcssserver-$VERSION.tar.gz && \
    cd rcssserver-rcssserver-$VERSION && \
    ./bootstrap && \
    ./configure && \
    make && \
    make install && \
    ldconfig

RUN wget https://github.com/rcsoccersim/rcssmonitor/archive/rcssmonitor-$VERSION.tar.gz && \
    tar xfz rcssmonitor-$VERSION.tar.gz && \
    cd rcssmonitor-rcssmonitor-$VERSION && \
    ./bootstrap && \
    ./configure && \
    make && \
    make install && \
    ldconfig

FROM ubuntu:18.04
COPY --from=build /usr/local/lib /usr/local/lib
COPY --from=build /usr/local/bin /usr/local/bin
RUN ldconfig && \
    apt update && \
    apt install -y libboost-filesystem1.65.1 libboost-system1.65.1 libboost-program-options-dev tmux
    
RUN apt-get install -y libqt5widgets5

# Python installs
RUN apt-get install 'python-dev'\
    'python3-pip'\
    'python-numpy'  -y

# Pip
RUN apt-get update && apt-get install -y \
    python3-pip
    
# Install python requirements
ADD requirements.txt /
RUN pip3 install -r ./requirements.txt
