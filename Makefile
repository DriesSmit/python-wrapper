all:
	xhost +
	docker run -it --rm -e DISPLAY="$$DISPLAY" --env QT_X11_NO_MITSHM=1 -v /tmp/.X11-unix:/tmp/.X11-unix:ro -v ${PWD}:/soccer robo2d:custom python3 soccer/robocup-soccer-master/main.py

old:
	xhost +
	docker run -it --rm -e DISPLAY="$$DISPLAY" --env QT_X11_NO_MITSHM=1 -v /tmp/.X11-unix:/tmp/.X11-unix:ro -v ${PWD}:/soccer robo2d:old python3 soccer/robocup-soccer-master/main.py

bash:
	docker run -it --rm robo2d:custom bash

build_old:
	docker build -t robo2d:old .
	
build:
	docker build -t robo2d:custom ./Custom/.

check_your_privilege:
	sudo chmod 777 . -R

