#!/usr/bin/env python3

import sys
# insert at 1, 0 is the script path (or '' in REPL)
sys.path.insert(1, '/soccer/robocup-soccer-master/aigent/soccerpy')

import threading
import time
import random

import sock
import sp_exceptions
import handler
from world_model import WorldModel

import os
os.environ['PYGAME_HIDE_SUPPORT_PROMPT'] = "hide"
import pygame

# Define some colours
green = (0, 255, 50)
light_blue = (0, 255, 255)
blue = (0, 0, 255)
red = (255, 0, 0)
yellow = (255, 255, 0)
white = (255, 255, 255)

# R G B
field_col = (0, 200, 50)
team_player = blue
opp_player = light_blue
own_goal = (255, 140, 0)
opp_goal = (128, 0, 128)

class Agent:
    def __init__(self, render_agent_view=False):
        # whether we're connected to a server yet or not
        self.__connected = False

        # set all variables and important objects to appropriate values for
        # pre-connect state.

        # the socket used to communicate with the server
        self.__sock = None

        # Render the agent's view for debugging purposes
        self.render_agent_view = render_agent_view

        if render_agent_view:
            # initialize game engine

            os.environ['SDL_VIDEO_CENTERED'] = '1'
            pygame.init()
            self.clock = pygame.time.Clock()

            # Open a window
            self.screen_size = (800, 500)

            self.r_to_s = 3

            self.screen = pygame.display.set_mode(self.screen_size)

            # Set title to the window
            pygame.display.set_caption("Player view")
            # For rendering


        # models and the message handler for parsing and storing information
        self.wm = None
        self.msg_handler = None

        # parse thread and control variable
        self.__parsing = False
        self.__msg_thread = None

        self.__thinking = False # think thread and control variable
        self.__think_thread = None

        # whether we should run the think method
        self.__should_think_on_data = False

        # whether we should send commands
        self.__send_commands = False

        # adding goal post markers
        self.enemy_goal_pos = None
        self.own_goal_pos = None

    def connect(self, host, port, teamname, version=11):
        """
        Gives us a connection to the server as one player on a team.  This
        immediately connects the agent to the server and starts receiving and
        parsing the information it sends.
        """

        # if already connected, raise an error since user may have wanted to
        # connect again to a different server.
        if self.__connected:
            msg = "Cannot connect while already connected, disconnect first."
            raise sp_exceptions.AgentConnectionStateError(msg)

        # the pipe through which all of our communication takes place
        self.__sock = sock.Socket(host, port)

        # our models of the world and our body
        self.wm = WorldModel(handler.ActionHandler(self.__sock))

        # set the team name of the world model to the given name
        self.wm.teamname = teamname

        # handles all messages received from the server
        self.msg_handler = handler.MessageHandler(self.wm)

        # set up our threaded message receiving system
        self.__parsing = True # tell thread that we're currently running
        self.__msg_thread = threading.Thread(target=self.__message_loop,
                name="message_loop")
        self.__msg_thread.daemon = True # dies when parent thread dies

        # start processing received messages. this will catch the initial server
        # response and all subsequent communication.
        self.__msg_thread.start()

        # send the init message and allow the message handler to handle further
        # responses.
        init_address = self.__sock.address
        init_msg = "(init %s (version %d))"
        self.__sock.send(init_msg % (teamname, version))

        # wait until the socket receives a response from the server and gets its
        # assigned port.
        while self.__sock.address == init_address:
            time.sleep(0.0001)

        # create our thinking thread.  this will perform the actions necessary
        # to play a game of robo-soccer.
        self.__thinking = False
        # self.__think_thread = threading.Thread(target=self.__think_loop,
        #         name="think_loop")
        # self.__think_thread.daemon = True

        # set connected state.  done last to prevent state inconsistency if
        # something goes wrong beforehand.
        self.__connected = True

    def play(self):
        """
        Kicks off the thread that does the agent's thinking, allowing it to play
        during the game.  Throws an exception if called while the agent is
        already playing.
        """

        # ensure we're connected before doing anything
        if not self.__connected:
            msg = "Must be connected to a server to begin play."
            raise sp_exceptions.AgentConnectionStateError(msg)

        # throw exception if called while thread is already running
        if self.__thinking:
            raise sp_exceptions.AgentAlreadyPlayingError(
                "Agent is already playing.")

        # run the method that sets up the agent's persistant variables
        self.setup_environment()

        # tell the thread that it should be running, then start it
        self.__thinking = True
        self.__should_think_on_data = True
        self.__think_loop()
        #self.__think_thread.start()

    def disconnect(self):
        """
        Tell the loop threads to stop and signal the server that we're
        disconnecting, then join the loop threads and destroy all our inner
        methods.

        Since the message loop thread can conceiveably block indefinitely while
        waiting for the server to respond, we only allow it (and the think loop
        for good measure) a short time to finish before simply giving up.

        Once an agent has been disconnected, it is 'dead' and cannot be used
        again.  All of its methods get replaced by a method that raises an
        exception every time it is called.
        """

        # don't do anything if not connected
        if not self.__connected:
            return

        # tell the loops to terminate
        self.__parsing = False
        self.__thinking = False

        # tell the server that we're quitting
        self.__sock.send("(bye)")

        # tell our threads to join, but only wait breifly for them to do so.
        # don't join them if they haven't been started (this can happen if
        # disconnect is called very quickly after connect).
        if self.__msg_thread.is_alive():
            self.__msg_thread.join(0.01)

        # if self.__think_thread.is_alive():
        #     self.__think_thread.join(0.01)

        # reset all standard variables in this object.  self.__connected gets
        # reset here, along with all other non-user defined internal variables.
        Agent.__init__(self)

    def __message_loop(self):
        """
        Handles messages received from the server.

        This SHOULD NOT be called externally, since it's used as a threaded loop
        internally by this object.  Calling it externally is a BAD THING!
        """

        # loop until we're told to stop
        while self.__parsing:
            # receive message data from the server and pass it along to the
            # world model as-is.  the world model parses it and stores it within
            # itself for perusal at our leisure.
            raw_msg = self.__sock.recv()
            msg_type = self.msg_handler.handle_message(raw_msg)

            # we send commands all at once every cycle, ie. whenever a
            # 'sense_body' command is received
            if msg_type == handler.ActionHandler.CommandType.SENSE_BODY:
                self.__send_commands = True

            # print("Here1")

            # flag new data as needing the think loop's attention
            self.__should_think_on_data = True

    def __render_view(self):
        # print("Outputs: ", self.wm)

        # print("abs_coords: ", self.wm.abs_coords)
        # print("abs_neck_dir: ", self.wm.abs_neck_dir)
        # print("abs_body_dir: ", self.wm.abs_body_dir)

        # if self.wm.ball is not None:
        #     print("ball.direction: ", self.wm.ball.direction)
        #     print("ball.distance: ", self.wm.ball.distance)

        field_surf = pygame.Surface(self.screen_size)
        field_surf.fill(field_col)

        pygame.draw.circle(field_surf, yellow,
                           (int(self.screen_size[0] / 2), int(self.screen_size[1] - 100)),
                           5)

        # self.ball = None
        # self.flags = []
        # self.goals = []
        # self.players = []
        # self.lines = []

        # Plot flags
        if self.wm.flags is not None:
            for flag in self.wm.flags:
                if flag is not None and flag.direction is not None and flag.distance is not None:
                    flag_coords = self.__rel_to_plot_coords(flag)
                    pygame.draw.circle(field_surf, red, flag_coords, 2)

        # Plot lines
        # if self.wm.lines is not None:
        #     for line in self.wm.lines:
        #         if line is not None and line.direction is not None and line.distance is not None:
        #             flag_coords = self.__rel_to_plot_coords(line)
        #             pygame.draw.circle(field_surf, blue, flag_coords, 2)

        # Plot goals
        if self.wm.goals is not None:
            for goal in self.wm.goals:
                if goal is not None and goal.direction is not None and goal.distance is not None:
                    goal_coords = self.__rel_to_plot_coords(goal)

                    if goal.goal_id == self.wm.side:
                        pygame.draw.circle(field_surf, own_goal, goal_coords, 2)
                    else:
                        pygame.draw.circle(field_surf, opp_goal, goal_coords, 2)
        # Plot players
        if self.wm.players is not None:
            for player in self.wm.players:
                if player is not None and player.direction is not None and player.distance is not None:
                    player_coords = self.__rel_to_plot_coords(player)

                    if player.side == self.wm.side:
                        pygame.draw.circle(field_surf, team_player, player_coords, 2)
                    else:
                        pygame.draw.circle(field_surf, opp_player, player_coords, 2)

        if self.wm.ball is not None and self.wm.ball.distance and self.wm.ball.direction:
            # a = 0.25 * (self.wm.ball.direction / 180)
            # b = 0.25 * (self.wm.ball.distance)

            # ball_coords = self.wm.get_object_absolute_coords(self.wm.ball)

            # pygame.draw.circle(field_surf, white,
            #                    (int(self.screen_size[0] / 2), int(self.screen_size[1] / 2)),
            #                    int(self.wm.ball.distance*self.r_to_s), 2)

            # Plot ball relative to player
            ball_coords = self.__rel_to_plot_coords(self.wm.ball)
            pygame.draw.circle(field_surf, white, ball_coords, 2)

            # pygame.draw.line(field_surf, white, (
            #     self.cen_screen_x - 1, self.cen_screen_y_1), (self.cen_screen_x - 1, self.cen_screen_y_2),
            #                  2)
            #                    (int(self.screen_size[0] / 2 + ball_coords[0]),
            #                     int(self.screen_size[1] / 2 - ball_coords[1])), 2)

            # pygame.draw.circle(field_surf, white,
            #                    (int(self.screen_size[0] / 2 + ball_coords[0]),
            #                     int(self.screen_size[1] / 2 - ball_coords[1])), 2)

        self.screen.blit(field_surf, [0, 0])
        pygame.display.flip()

    def __register_keys(self):
        keys = pygame.key.get_pressed()
        pygame.event.pump()
        if keys[pygame.K_w]:
            # print("Move forward")
            self.wm.ah.dash(50)
        # elif keys[pygame.K_s]:
        #     print("Turn right")

        if keys[pygame.K_a]:
            # print("Rotate left")
            self.wm.ah.turn(-10)
        elif keys[pygame.K_d]:
            # print("Rotate right")
            self.wm.ah.turn(10)

        elif keys[pygame.K_e]:
            # print("Rotate right")
            if self.wm.ball is not None and self.wm.ball.direction is not None and self.wm.ball.distance is not None:
                self.wm.kick_to((55, 0), 0.0)

    def __rel_to_plot_coords(self, obj):
        import math
        img_cen = int(self.screen_size[0] / 2), int(self.screen_size[1] - 100)
        obj_x = -obj.distance * math.sin(-obj.direction*math.pi/180)
        obj_y = -obj.distance * math.cos(-obj.direction*math.pi/180)
        return int(img_cen[0] + obj_x*self.r_to_s), int(img_cen[1] + obj_y*self.r_to_s)

    def __fixed_think(self):
        pass

    def __think_loop(self):
        """
        Performs world model analysis and sends appropriate commands to the
        server to allow the agent to participate in the current game.

        Like the message loop, this SHOULD NOT be called externally.  Use the
        play method to start play, and the disconnect method to end it.
        """

        # while self.__thinking:
        #     # only think if new data has arrived
        #     if self.__should_think_on_data:
        #         # flag that data has been processed.  this shouldn't be a race
        #         # condition, since the only change would be to make it True
        #         # before changing it to False again, and we're already going to
        #         # process data, so it doesn't make any difference.
        #         #print("Thinking..")
        #         self.__should_think_on_data = False
        #         self.__sock.send("(turn 30)\x00",append_null_terminator=False)
        #         # time.sleep(0.01)
        #         self.__sock.send("(done)\x00", append_null_terminator=False)
        #
        # exit()
        while self.__thinking:
            # tell the ActionHandler to send its enqueued messages if it is time
            if self.__send_commands:
                self.__send_commands = False
                self.wm.ah.send_commands()

            # only think if new data has arrived
            if self.__should_think_on_data:
                # flag that data has been processed.  this shouldn't be a race
                # condition, since the only change would be to make it True
                # before changing it to False again, and we're already going to
                # process data, so it doesn't make any difference.
                self.__should_think_on_data = False

                #self.__sock.send("(turn 30)\x00", append_null_terminator=False)

                # Register keys
                if self.render_agent_view:
                    self.__register_keys()

                # Update agent view
                if self.render_agent_view and self.wm.abs_neck_dir is not None:
                    self.__render_view()

                # performs the actions necessary for the agent to play soccer
                if not self.render_agent_view:
                    # self.__fixed_think()
                    self.think()
            else:
                # prevent from burning up all the cpu time while waiting for data
                time.sleep(0.0001)

    def setup_environment(self):
        """
        Called before the think loop starts, this allows the user to store any
        variables/objects they'll want access to across subsequent calls to the
        think method.
        """

        self.in_kick_off_formation = False

    def think(self):
        """
        Performs a single step of thinking for our agent.  Gets called on every
        iteration of our think loop.
        """

        # print("Here2")

        # DEBUG:  tells us if a thread dies
        if not self.__msg_thread.is_alive():
            raise Exception("A thread died.")

        # take places on the field by uniform number
        if not self.in_kick_off_formation:

            # used to flip x coords for other side
            side_mod = 1
            if self.wm.side == WorldModel.SIDE_R:
                side_mod = -1

            if self.wm.uniform_number == 1:
                self.wm.teleport_to_point((-5 * side_mod, 30))
            elif self.wm.uniform_number == 2:
                self.wm.teleport_to_point((-40 * side_mod, 15))
            elif self.wm.uniform_number == 3:
                self.wm.teleport_to_point((-40 * side_mod, 00))
            elif self.wm.uniform_number == 4:
                self.wm.teleport_to_point((-40 * side_mod, -15))
            elif self.wm.uniform_number == 5:
                self.wm.teleport_to_point((-5 * side_mod, -30))
            elif self.wm.uniform_number == 6:
                self.wm.teleport_to_point((-20 * side_mod, 20))
            elif self.wm.uniform_number == 7:
                self.wm.teleport_to_point((-20 * side_mod, 0))
            elif self.wm.uniform_number == 8:
                self.wm.teleport_to_point((-20 * side_mod, -20))
            elif self.wm.uniform_number == 9:
                self.wm.teleport_to_point((-10 * side_mod, 0))
            elif self.wm.uniform_number == 10:
                self.wm.teleport_to_point((-10 * side_mod, 20))
            elif self.wm.uniform_number == 11:
                self.wm.teleport_to_point((-10 * side_mod, -20))

            self.in_kick_off_formation = True

            return

        # determine the enemy goal position
        goal_pos = None
        if self.wm.side == WorldModel.SIDE_R:
            goal_pos = (-55, 0)
        else:
            goal_pos = (55, 0)

        # print(goal_pos)

        # kick off!
        if self.wm.is_before_kick_off():
            # player 9 takes the kick off
            if self.wm.uniform_number == 9:
                if self.wm.is_ball_kickable():
                    # kick with 100% extra effort at enemy goal
                    self.wm.kick_to(goal_pos, 1.0)
                else:
                    # move towards ball
                    if self.wm.ball is not None:
                        if (self.wm.ball.direction is not None and
                                -7 <= self.wm.ball.direction <= 7):
                            self.wm.ah.dash(50)
                        else:
                            self.wm.turn_body_to_point((0, 0))

                # turn to ball if we can see it, else face the enemy goal
                if self.wm.ball is not None:
                    self.wm.turn_neck_to_object(self.wm.ball)

                return

        # attack!
        else:


            # kick it at the enemy goal

            # Get opponent goal coordinates
            # opp_goal = None
            # for goal in self.wm.goals:
            #     if goal.goal_id != self.wm.side:
            #         opp_goal = goal
            #         break

            # print(self.wm.speed_direction)

            if self.wm.ball is None or self.wm.ball.direction is None:
                    self.wm.ah.turn(30)
                    return
            elif self.wm.is_ball_kickable():
                self.wm.kick_to(goal_pos, 0.0)
                return
            else:
                # move towards ball
                if -7 <= self.wm.ball.direction <= 7:
                    # if self.wm.ball.distance > 5:
                    self.wm.ah.dash(65)
                    # else:
                    #     pass
                        # and -7 <= self.wm.opp_goal.direction <= 7
                else:
                    # face ball
                    self.wm.ah.turn(self.wm.ball.direction / 2)

                return