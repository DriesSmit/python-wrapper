#!/usr/bin/env python

import threading
import time
import random
import sys
import multiprocessing as mp
import os

import sys
# insert at 1, 0 is the script path (or '' in REPL)
sys.path.insert(1, '/soccer/robocup-soccer-master/aigent')

# import agent types (positions)
# from aigent.soccerpy.agent import Agent as A0

#human controller
from soccerpy.agent import Agent as basic_agent
from soccerpy.trainer import Trainer

# strikers
# from aigent.agent_1 import Agent as striker
# defenders
# from aigent.agent_2 import Agent as defender
# goalie
# from aigent.agent_3 import Agent as goalie

# set team
render_view = True

# return type of agent: midfield, striker etc.
def agent_type(position):
    return basic_agent
    # return {
    #     2: defender,
    #     3: goalie,
    #     4: defender,
    #     6: defender,
    #     7: defender,
    #     8: defender,
    # }.get(position, striker)

# spawn an agent of team_name, with position
def spawn_agent(team_name, position, render_agent_view):
    """
    Used to run an agent in a seperate physical process.
    """
    # return type of agent by position, construct
    a = agent_type(position)(render_agent_view)
    a.connect("localhost", 6000, team_name)
    a.play()

    # we wait until we're killed
    while 1:
        # we sleep for a good while since we can only exit if terminated.
        time.sleep(1)

def run_server_thread(mode):
    # ./usr/local/bin/rcssmonitor
    if mode == 'play':
        os.system('./usr/local/bin/rcssserver -server::synch_mode=false') # -server::coach=off -server::synch_mode=true
    elif mode == "train":
        os.system('./usr/local/bin/rcssserver -server::coach_w_referee=on -server::synch_mode=true') # -server::half_time=100
    else:
        raise ValueError

def run_trainer_thread():
    a = Trainer()
    a.connect("localhost", 6001)
    a.run()

    # we wait until we're killed
    while 1:
        # we sleep for a good while since we can only exit if terminated.
        time.sleep(1)

def run_monitor_thread():
    # ./usr/local/bin/rcssmonitor
    os.system('./usr/local/bin/rcssmonitor')

# spawn an agent of team_name, with position
def start_trainer():
    # Wait for server to startup completely
    at = mp.Process(target=run_trainer_thread)
    at.daemon = True
    at.start()


def start_server(mode="play"):
    # Wait for server to startup completely
    print("mode: ", mode)
    at = mp.Process(target=run_server_thread, args=(mode,))
    at.daemon = True
    at.start()
    time.sleep(0.1)

def start_monitor():
    # Wait for monitor process to complete
    at = mp.Process(target=run_monitor_thread)
    at.daemon = True
    at.start()
    time.sleep(0.1)

def connect_agents(team_name,num_players):
    # spawn all agents as seperate processes for maximum processing efficiency
    agentthreads = []
    for position in range(1, num_players + 1):
        print("  Spawning agent %d..." % position)

        at = mp.Process(target=spawn_agent, args=(team_name, position, render_view))
        at.daemon = True
        at.start()
        agentthreads.append(at)

    print("Spawned %d agents." % len(agentthreads))

if __name__ == "__main__":

    game_type = "play"  # "play"
                        # "train"

    if game_type == "train":
        # Play game of soccer using official RoboCup 2D setup. The monitor is the RoboCup monitor.
        start_server("train")
        start_trainer()
        start_monitor()
        connect_agents(team_name='DeepEntropy', num_players=11)
        connect_agents(team_name='DeepOrder', num_players=11)

        # wait until killed to terminate agent processes
        try:
            while 1:
                time.sleep(0.05)
        except KeyboardInterrupt:
            print("Exiting.")
            sys.exit()
    elif game_type == "play":
        # Play game of soccer using official RoboCup 2D setup. The monitor is the RoboCup monitor.
        start_server()
        start_monitor()
        connect_agents(team_name='DeepEntropy', num_players=1)
        connect_agents(team_name='DeepOrder', num_players=1)

        print()
        print("Playing soccer...")

        # wait until killed to terminate agent processes
        try:
            while 1:
                time.sleep(0.05)
        except KeyboardInterrupt:
            # print()
            # print("Killing agent threads...")
            #
            # # terminate all agent processes
            # count = 0
            # for at in agentthreads:
            #     print("  Terminating agent %d..." % count)
            #     at.terminate()
            #     count += 1
            # print("Killed %d agent threads." % (count - 1))
            #
            # print()
            print("Exiting.")
            sys.exit()
    else:
        raise NotImplementError("game_type not implemented: ", game_type)


